/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics.exchange.index;

import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.IndexExchangeRepository;
import com.kay.clouds.persistence.analytics.DataWarehouseHelper;
import com.kay.clouds.persistence.analytics.IndexExchangeTable;
import com.kay.clouds.persistence.analytics.exchange.CassandraExchangeRepository;

/**
 * @inheritDoc
 * @author Lili
 */
public class CassandraIndexExchangeRepository
        extends CassandraExchangeRepository<Exchange, CassandraIndexExchange>
        implements IndexExchangeRepository {

    public CassandraIndexExchangeRepository(DataWarehouseHelper dataSourceHelper) {
        super(dataSourceHelper);
        mapper = dataSourceHelper.getDataMapper(CassandraIndexExchange.class);
        session = mapper.getManager().getSession();
        idColumn = IndexExchangeTable.TICKER_COLUMN;
        indexColumn = IndexExchangeTable.DATE_COLUMN;
        tableName = IndexExchangeTable.TABLE_NAME;
    }

    @Override
    protected CassandraIndexExchange parse(Exchange entity) {
        return CassandraIndexExchange.clone(entity);
    }

}
