/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics;

import com.datastax.driver.mapping.annotations.Accessor;
import com.kay.clouds.persistence.CassandraConstants;
import static com.kay.clouds.persistence.CassandraConstants.CASSANDRA_VALUE_TYPE_DATE;
import static com.kay.clouds.persistence.CassandraConstants.CASSANDRA_VALUE_TYPE_DOUBLE;
import static com.kay.clouds.persistence.CassandraConstants.CASSANDRA_VALUE_TYPE_STRING;
import static com.kay.clouds.persistence.analytics.DataWarehouseHelper.DATA_WAREHOUSE;

/**
 *
 * @author Lili
 */
@Accessor
public interface StockExchangeTable {

    public static final String TABLE_NAME = "stockDailyExchange";
    public static final String TICKER_COLUMN = CassandraConstants.TICKER_COLUMN;
    public static final String DATE_COLUMN = "date";
    public static final String OPENNING_PRICE_COLUMN = "openningPrice";
    public static final String DAILY_MAXPRICE_COLUMN = "dailyMaxPrice";
    public static final String DAILY_MINPRICE_COLUMN = "dailyMinPrice";
    public static final String CLOSINGPRICE_COLUMN = "closingPrice";
    public static final String EXCHANGE_VOLUME_COLUMN = "exchangeVolume";
    public static final String ADJUSTED_CLOSINGPRICE_COLUMN = "adjustedclosingPrice";
    public static final String CURRENCY_CODE_COLUMN = "currencycode";

    public static String getExchangeTableScheme() {
        return "CREATE TABLE IF NOT EXISTS "
                + DATA_WAREHOUSE + "." + TABLE_NAME + " ("
                + "PRIMARY KEY (" + TICKER_COLUMN + "," + DATE_COLUMN + ") ,"
                + TICKER_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + DATE_COLUMN + " " + CASSANDRA_VALUE_TYPE_DATE + ","
                + OPENNING_PRICE_COLUMN + " " + CASSANDRA_VALUE_TYPE_DOUBLE + ","
                + DAILY_MAXPRICE_COLUMN + " " + CASSANDRA_VALUE_TYPE_DOUBLE + ","
                + DAILY_MINPRICE_COLUMN + " " + CASSANDRA_VALUE_TYPE_DOUBLE + ","
                + CLOSINGPRICE_COLUMN + " " + CASSANDRA_VALUE_TYPE_DOUBLE + ","
                + EXCHANGE_VOLUME_COLUMN + " " + CASSANDRA_VALUE_TYPE_DOUBLE + ","
                + ADJUSTED_CLOSINGPRICE_COLUMN + " " + CASSANDRA_VALUE_TYPE_DOUBLE
                + ");";
    }

}
