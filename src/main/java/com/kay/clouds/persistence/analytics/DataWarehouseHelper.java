/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kay.clouds.persistence.CassandraConstants;

/**
 *
 * @author Lili
 */
public final class DataWarehouseHelper
        implements CassandraConstants {

    public static final String DATA_WAREHOUSE = "StockDataWareHouse";

    private final Session session;

    private DataWarehouseHelper(Session session) {
        this.session = session;
        createStockExchangeKeySpace();
    }

    public static DataWarehouseHelper getCassandraDataSourceHelper(String... ips) {

        return getCassandraDataSourceHelper(CASSANDRA_DEFAULT_PORT, ips);
    }

    public static DataWarehouseHelper getCassandraDataSourceHelper(Integer port, String... ips) {

        Cluster cluster = Cluster.builder()
                .withPort(port)
                .addContactPoints(ips)
                .build();

        return new DataWarehouseHelper(cluster.newSession());
    }

    public void createStockExchangeKeySpace() {
        session.execute("CREATE KEYSPACE IF NOT EXISTS " + DATA_WAREHOUSE
                + " WITH replication " + "= {'class':'SimpleStrategy', 'replication_factor':1};");
        session.execute(StockExchangeTable.getExchangeTableScheme());
        session.execute(IndexExchangeTable.getExchangeTableScheme());
    }

    public <T> Mapper<T> getDataMapper(Class<T> classType) {
        return new MappingManager(session).mapper(classType);
    }

    public <A> A createAccessor(Class<A> classType) {
        return new MappingManager(session).createAccessor(classType);
    }
}
