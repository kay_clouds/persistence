/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics.exchange;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.kay.clouds.domain.analytics.exchange.Exchange;
import java.util.Date;

/**
 *
 * @inheritDoc
 *
 * @author Lili
 */
public class CassandraExchange implements Exchange {

    @PartitionKey(value = 0)
    protected String ticker;
    @PartitionKey(value = 1)
    protected Date date;
    protected Double openningPrice;
    protected Double dailyMaxPrice;
    protected Double dailyMinPrice;
    protected Double closingPrice;
    protected Double exchangeVolume;
    protected Double adjustedclosingPrice;

    @Override
    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    @Override
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public Double getOpenningPrice() {
        return openningPrice;
    }

    public void setOpenningPrice(Double openningPrice) {
        this.openningPrice = openningPrice;
    }

    @Override
    public Double getDailyMaxPrice() {
        return dailyMaxPrice;
    }

    public void setDailyMaxPrice(Double dailyMaxPrice) {
        this.dailyMaxPrice = dailyMaxPrice;
    }

    @Override
    public Double getDailyMinPrice() {
        return dailyMinPrice;
    }

    public void setDailyMinPrice(Double dailyMinPrice) {
        this.dailyMinPrice = dailyMinPrice;
    }

    @Override
    public Double getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(Double closingPrice) {
        this.closingPrice = closingPrice;
    }

    @Override
    public Double getExchangeVolume() {
        return exchangeVolume;
    }

    public void setExchangeVolume(Double exchangeVolume) {
        this.exchangeVolume = exchangeVolume;
    }

    @Override
    public Double getAdjustedclosingPrice() {
        return adjustedclosingPrice;
    }

    public void setAdjustedclosingPrice(Double adjustedclosingPrice) {
        this.adjustedclosingPrice = adjustedclosingPrice;
    }

}
