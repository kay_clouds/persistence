/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics.exchange.index;

import com.datastax.driver.mapping.annotations.Table;
import com.kay.clouds.domain.analytics.exchange.Exchange;
import static com.kay.clouds.persistence.analytics.DataWarehouseHelper.DATA_WAREHOUSE;
import com.kay.clouds.persistence.analytics.IndexExchangeTable;
import com.kay.clouds.persistence.analytics.exchange.CassandraExchange;

/**
 *
 * @inheritDoc
 *
 * @author Lili
 */
@Table(keyspace = DATA_WAREHOUSE, name = IndexExchangeTable.TABLE_NAME)
public class CassandraIndexExchange extends CassandraExchange implements Exchange {

    static CassandraIndexExchange clone(Exchange indexExchange) {
        CassandraIndexExchange cassandraIndexExchange = new CassandraIndexExchange();
        cassandraIndexExchange.ticker = indexExchange.getTicker();
        cassandraIndexExchange.date = indexExchange.getDate();
        cassandraIndexExchange.openningPrice = indexExchange.getOpenningPrice();
        cassandraIndexExchange.dailyMaxPrice = indexExchange.getDailyMaxPrice();
        cassandraIndexExchange.dailyMinPrice = indexExchange.getDailyMinPrice();
        cassandraIndexExchange.closingPrice = indexExchange.getClosingPrice();
        cassandraIndexExchange.exchangeVolume = indexExchange.getExchangeVolume();
        cassandraIndexExchange.adjustedclosingPrice = indexExchange.getAdjustedclosingPrice();
        return cassandraIndexExchange;
    }

}
