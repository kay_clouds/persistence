/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics.exchange;

import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.ExchangeRepository;
import com.kay.clouds.persistence.CassandraIndexRepository;
import com.kay.clouds.persistence.analytics.DataWarehouseHelper;
import java.util.Date;

/**
 * @inheritDoc
 * @author Lili
 */
public abstract class CassandraExchangeRepository<EXCHANGE extends Exchange, DATA extends EXCHANGE>
        extends CassandraIndexRepository<String, Date, EXCHANGE, DATA>
        implements ExchangeRepository<EXCHANGE> {

    public CassandraExchangeRepository(DataWarehouseHelper dataSourceHelper) {
        this.keySpace = DataWarehouseHelper.DATA_WAREHOUSE;
    }

}
