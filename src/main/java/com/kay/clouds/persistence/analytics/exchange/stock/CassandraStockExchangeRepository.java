/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics.exchange.stock;

import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.StockExchangeRepository;
import com.kay.clouds.persistence.analytics.DataWarehouseHelper;
import com.kay.clouds.persistence.analytics.StockExchangeTable;
import com.kay.clouds.persistence.analytics.exchange.CassandraExchangeRepository;

/**
 *
 * @author Lili
 */
public class CassandraStockExchangeRepository
        extends CassandraExchangeRepository<Exchange, CassandraStockExchange>
        implements StockExchangeRepository {

    public CassandraStockExchangeRepository(DataWarehouseHelper dataSourceHelper) {
        super(dataSourceHelper);
        mapper = dataSourceHelper.getDataMapper(CassandraStockExchange.class);
        session = mapper.getManager().getSession();
        idColumn = StockExchangeTable.TICKER_COLUMN;
        indexColumn = StockExchangeTable.DATE_COLUMN;
        tableName = StockExchangeTable.TABLE_NAME;
    }

    @Override
    protected CassandraStockExchange parse(Exchange entity) {
        return CassandraStockExchange.clone(entity);
    }

}
