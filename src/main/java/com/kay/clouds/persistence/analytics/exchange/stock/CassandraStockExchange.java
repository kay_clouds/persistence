/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics.exchange.stock;

import com.datastax.driver.mapping.annotations.Table;
import com.kay.clouds.domain.analytics.exchange.Exchange;
import static com.kay.clouds.persistence.analytics.DataWarehouseHelper.DATA_WAREHOUSE;
import static com.kay.clouds.persistence.analytics.StockExchangeTable.TABLE_NAME;
import com.kay.clouds.persistence.analytics.exchange.CassandraExchange;

/**
 *
 * @inheritDoc
 *
 * @author Lili
 */
@Table(keyspace = DATA_WAREHOUSE, name = TABLE_NAME)
public class CassandraStockExchange extends CassandraExchange implements Exchange {

    static CassandraStockExchange clone(Exchange stockExchange) {
        CassandraStockExchange cassandraStockExchange = new CassandraStockExchange();
        cassandraStockExchange.ticker = stockExchange.getTicker();
        cassandraStockExchange.date = stockExchange.getDate();
        cassandraStockExchange.openningPrice = stockExchange.getOpenningPrice();
        cassandraStockExchange.dailyMaxPrice = stockExchange.getDailyMaxPrice();
        cassandraStockExchange.dailyMinPrice = stockExchange.getDailyMinPrice();
        cassandraStockExchange.closingPrice = stockExchange.getClosingPrice();
        cassandraStockExchange.exchangeVolume = stockExchange.getExchangeVolume();
        cassandraStockExchange.adjustedclosingPrice = stockExchange.getAdjustedclosingPrice();
        return cassandraStockExchange;
    }

}
