package com.kay.clouds.persistence;

import com.kay.clouds.domain.Entity;
import com.kay.clouds.domain.Relation;
import java.util.Date;

/**
 *
 * @author Lili
 * @param <FROM>
 * @param <TO>
 */
public class CassandraRelation<FROM extends Entity, TO extends Entity> implements Relation<FROM, TO> {

    private FROM from;
    private TO to;
    private Date creation;

    @Override
    public FROM getFrom() {
        return from;
    }

    public CassandraRelation<FROM, TO> from(FROM from) {
        this.from = from;
        return this;
    }

    @Override
    public TO getTo() {
        return to;
    }

    public CassandraRelation<FROM, TO> to(TO to) {
        this.to = to;
        return this;
    }

    @Override
    public Date getCreation() {
        return creation;
    }

    public CassandraRelation<FROM, TO> creation(Date creation) {
        this.creation = creation;
        return this;
    }

}
