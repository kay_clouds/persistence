package com.kay.clouds.persistence;

import java.util.Date;

/**
 *
 * @author Lili
 * @param <K>
 * @param <V>
 */
public interface CassandraRelationKey<K, V> {

    public K fromId();

    public V toId();

    public Date creation();
}
