/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.market.index.contain;

import com.kay.clouds.persistence.platform.MarketContainIndexTable;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.market.index.CassandraMarketIndexRepository;
import java.util.Date;
import com.kay.clouds.domain.platform.market.index.MarketContainIndexRepository;

/**
 * @inheritDoc
 * @author Lili
 */
public class CassandraMarketContainIndexRepository
        extends CassandraMarketIndexRepository<CassandraMarketContainIndex>
        implements MarketContainIndexRepository {

    public CassandraMarketContainIndexRepository(PlatformDataBaseHelper dataSourceHelper) {
        super(dataSourceHelper);
        fromKeyColumn = MarketContainIndexTable.FROM_COLUMN;
        tableName = MarketContainIndexTable.TABLE_NAME;
        relationKeyMapper = dataSourceHelper.getDataMapper(CassandraMarketContainIndex.class);
        session = relationKeyMapper.getManager().getSession();
    }

    @Override
    protected CassandraMarketContainIndex newKey(String fromKey, String toKey) {
        CassandraMarketContainIndex contain = new CassandraMarketContainIndex();
        contain.setCreationTime(new Date());
        contain.setMarketId(fromKey);
        contain.setIndexTicker(toKey);
        return contain;
    }
}
