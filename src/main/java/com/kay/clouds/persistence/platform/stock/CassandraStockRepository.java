/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.stock;

import com.kay.clouds.domain.platform.stock.Stock;
import com.kay.clouds.domain.platform.stock.StockRepository;
import com.kay.clouds.persistence.CassandraEntityRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.StockTable;

/**
 *
 * @author Lili
 */
public class CassandraStockRepository
        extends CassandraEntityRepository<String, Stock, CassandraStock>
        implements StockRepository {

    public CassandraStockRepository(PlatformDataBaseHelper dataSourceHelper) {
        mapper = dataSourceHelper.getDataMapper(CassandraStock.class);
        session = mapper.getManager().getSession();
        keySpace = PlatformDataBaseHelper.DATA_BASE;
        tableName = StockTable.TABLE_NAME;
        idColumn = StockTable.TICKER_COLUMN;
    }

    @Override
    protected CassandraStock parse(Stock entity) {
        return CassandraStock.clone(entity);
    }

}
