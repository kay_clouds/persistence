/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.market.index;

import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.platform.index.Index;
import com.kay.clouds.domain.platform.market.Market;
import com.kay.clouds.domain.platform.market.index.MarketIndexRepository;
import com.kay.clouds.persistence.CassandraRelationKey;
import com.kay.clouds.persistence.CassandraRelationRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.index.CassandraIndexRepository;
import com.kay.clouds.persistence.platform.market.CassandraMarketRepository;

/**
 * @inheritDoc
 * @author Lili
 *
 * @param <RELATION>
 */
public abstract class CassandraMarketIndexRepository< RELATION extends CassandraRelationKey<String, String>>
        extends CassandraRelationRepository<String, String, Market, Index, RELATION>
        implements MarketIndexRepository {

    public CassandraMarketIndexRepository(PlatformDataBaseHelper dataSourceHelper) {
        fromRepository = new CassandraMarketRepository(dataSourceHelper);
        toRepository = new CassandraIndexRepository(dataSourceHelper);
        keySpace = PlatformDataBaseHelper.DATA_BASE;
        fromError = ErrorCode.NOT_FOUND_MARKET;
        toError = ErrorCode.NOT_FOUND_INDEX;
    }

}
