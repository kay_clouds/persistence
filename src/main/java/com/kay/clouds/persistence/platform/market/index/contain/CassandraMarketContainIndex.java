/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.market.index.contain;

import com.datastax.driver.mapping.annotations.Table;
import com.kay.clouds.persistence.platform.MarketContainIndexTable;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;
import com.kay.clouds.persistence.platform.market.index.CassandraMarketIndex;

/**
 *
 * @inheritDoc
 *
 * @author Lili
 */
@Table(keyspace = DATA_BASE, name = MarketContainIndexTable.TABLE_NAME)
public class CassandraMarketContainIndex extends CassandraMarketIndex {

}
