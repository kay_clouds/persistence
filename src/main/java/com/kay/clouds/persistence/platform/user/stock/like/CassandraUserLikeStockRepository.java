/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.user.stock.like;

import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.UserLikeStockTable;
import com.kay.clouds.persistence.platform.user.stock.CassandraUserStockRepository;
import java.util.Date;
import java.util.UUID;
import com.kay.clouds.domain.platform.user.stock.UserLikeStockRepository;

/**
 *
 * @author daniel.eguia
 */
public class CassandraUserLikeStockRepository extends CassandraUserStockRepository<CassandraUserLikeStock>
        implements UserLikeStockRepository {

    public CassandraUserLikeStockRepository(PlatformDataBaseHelper dataSourceHelper) {
        super(dataSourceHelper);
        fromKeyColumn = UserLikeStockTable.FROM_COLUMN;
        tableName = UserLikeStockTable.TABLE_NAME;
        relationKeyMapper = dataSourceHelper.getDataMapper(CassandraUserLikeStock.class);
        session = relationKeyMapper.getManager().getSession();
    }

    @Override
    protected CassandraUserLikeStock newKey(UUID fromKey, String toKey) {
        CassandraUserLikeStock like = new CassandraUserLikeStock();
        like.setCreationTime(new Date());
        like.setStockTicker(toKey);
        like.setUserId(fromKey);
        return like;
    }

}
