/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform;

import com.kay.clouds.persistence.CassandraConstants;
import static com.kay.clouds.persistence.CassandraConstants.CASSANDRA_VALUE_TYPE_LONG;
import static com.kay.clouds.persistence.CassandraConstants.CASSANDRA_VALUE_TYPE_STRING;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.*;

/**
 *
 * @author Lili
 */
public interface StockTable {

    public static final String TABLE_NAME = "stock";
    public static final String TICKER_COLUMN = CassandraConstants.TICKER_COLUMN;
    public static final String COMPANY_COLUMN = "company";
    public static final String COMPANY_URL_COLUMN = "companyurl";
    public static final String SECTOR_COLUMN = "sector";
    public static final String MARKET_COLUMN = "market";
    public static final String SHARENUMBER_COLUMN = "sharenumber";

    /*As index is a key word of cassandra, we cannot use it alone,
    so we use stock index instead of index */
    public static final String INDEX_COLUMN = "stockIndex";
    public static final String INDUSTRY_COLUMN = "industry";
    public static final String COUNTRY_CODE_COLUMN = "countrycode";
    public static final String CURRENCY_CODE_COLUMN = "currencycode";

    public static String getTableScheme() {
        return "CREATE TABLE IF NOT EXISTS "
                + DATA_BASE + "." + TABLE_NAME + " ("
                + "PRIMARY KEY (" + TICKER_COLUMN + ") ,"
                + TICKER_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + COMPANY_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + COMPANY_URL_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + SECTOR_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + SHARENUMBER_COLUMN + " " + CASSANDRA_VALUE_TYPE_LONG + ","
                + MARKET_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + INDUSTRY_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + CURRENCY_CODE_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + COUNTRY_CODE_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING
                + ");";
    }

    public static String getStockMarketIndex() {
        return "CREATE INDEX IF NOT EXISTS stock_market ON  " + DATA_BASE + "." + TABLE_NAME
                + " (" + MARKET_COLUMN + " );";
    }

}
