/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.market.index;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.kay.clouds.persistence.CassandraRelationKey;
import java.util.Date;

/**
 *
 * @inheritDoc
 *
 * @author Lili
 */
public class CassandraMarketIndex implements CassandraRelationKey<String, String> {

    @PartitionKey(0)
    private String marketId;
    @PartitionKey(1)
    private String indexTicker;

    private Date creationTime;

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public String getIndexTicker() {
        return indexTicker;
    }

    public void setIndexTicker(String indexTicker) {
        this.indexTicker = indexTicker;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String fromId() {
        return marketId;
    }

    @Override
    public String toId() {
        return indexTicker;
    }

    @Override
    public Date creation() {
        return creationTime;
    }

}
