/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.user;

import com.kay.clouds.domain.platform.user.User;
import com.kay.clouds.domain.platform.user.UserRepository;
import com.kay.clouds.persistence.CassandraEntityRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.UserTable;
import java.util.UUID;

/**
 *
 * @author daniel.eguia
 */
public class CassandraUserRepository
        extends CassandraEntityRepository<UUID, User, CassandraUser>
        implements UserRepository {

    public CassandraUserRepository(PlatformDataBaseHelper dataSourceHelper) {
        mapper = dataSourceHelper.getDataMapper(CassandraUser.class);
        session = mapper.getManager().getSession();
        keySpace = PlatformDataBaseHelper.DATA_BASE;
        tableName = UserTable.TABLE_NAME;
        idColumn = UserTable.ID_COLUMN;
    }

    @Override
    protected CassandraUser parse(User entity) {
        return CassandraUser.clone(entity);
    }

}
