/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.index;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.kay.clouds.domain.platform.index.Index;
import com.kay.clouds.persistence.platform.IndexTable;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;

/**
 *
 * @author Lili
 */
@Table(keyspace = DATA_BASE, name = IndexTable.TABLE_NAME)
public class CassandraIndex implements Index {

    @PartitionKey
    private String ticker;

    private String name;

    private String currencyCode;

    private String description;

    @Override
    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    static CassandraIndex clone(Index index) {
        CassandraIndex cassandraIndex = new CassandraIndex();
        cassandraIndex.name = index.getName();
        cassandraIndex.currencyCode = index.getCurrencyCode();
        cassandraIndex.description = index.getDescription();
        cassandraIndex.ticker = index.getTicker();

        return cassandraIndex;
    }

}
