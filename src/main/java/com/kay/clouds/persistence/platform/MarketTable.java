/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform;

import static com.kay.clouds.persistence.CassandraConstants.CASSANDRA_VALUE_TYPE_DOUBLE;
import static com.kay.clouds.persistence.CassandraConstants.CASSANDRA_VALUE_TYPE_STRING;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.*;

/**
 *
 * @author Lili
 */
public interface MarketTable {

    /**
     * Table name.
     */
    public static final String TABLE_NAME = "market";
    /**
     * ISO 3166-1 ALFA 3, first key of the table.
     */
    public static final String COUNTRY_CODE_COLUMN = "countrycode";
    /**
     * Thomson Reuters Symbol as ID.
     */
    public static final String ID_COLUMN = "id";
    /**
     * Name in English.
     */
    public static final String NAME_COLUMN = "name";
    /**
     * ISO 4217 currency code.
     */
    public static final String CURRENCY_CODE_COLUMN = "currencycode";

    /**
     * Market description.
     */
    public static final String DESCRIPTION_COLUMN = "description";

    /**
     * Geography latitude.
     */
    public static final String LATITUED_COLUMN = "latitude";
    /**
     * Geography longitude.
     */
    public static final String LONGITUED_COLUMN = "longitude";

    static String getTableScheme() {
        return "CREATE TABLE IF NOT EXISTS "
                + DATA_BASE + "." + TABLE_NAME + " ("
                + "PRIMARY KEY (" + ID_COLUMN + ") ,"
                + COUNTRY_CODE_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + ID_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + NAME_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + CURRENCY_CODE_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + DESCRIPTION_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + LATITUED_COLUMN + " " + CASSANDRA_VALUE_TYPE_DOUBLE + ","
                + LONGITUED_COLUMN + " " + CASSANDRA_VALUE_TYPE_DOUBLE
                + ");";
    }

}
