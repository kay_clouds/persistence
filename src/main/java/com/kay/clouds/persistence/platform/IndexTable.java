/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform;

import com.kay.clouds.persistence.CassandraConstants;
import static com.kay.clouds.persistence.CassandraConstants.CASSANDRA_VALUE_TYPE_STRING;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.*;

/**
 *
 * @author Lili
 */
public interface IndexTable {

    /**
     * Summary of market in English.
     *
     * @return description of the market.
     */
    String getDescription();

    public static final String TABLE_NAME = "indexes";
    public static final String TICKER_COLUMN = CassandraConstants.TICKER_COLUMN;
    public static final String NAME_COLUMN = "name";
    public static final String CURRENCY_CODE_COLUMN = "currencycode";
    public static final String DESCRIPTION_COLUMN = "description";

    public static String getTableScheme() {
        return "CREATE TABLE IF NOT EXISTS "
                + DATA_BASE + "." + TABLE_NAME + " ("
                + "PRIMARY KEY (" + TICKER_COLUMN + ") ,"
                + TICKER_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + NAME_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + CURRENCY_CODE_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + DESCRIPTION_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING
                + ");";
    }

}
