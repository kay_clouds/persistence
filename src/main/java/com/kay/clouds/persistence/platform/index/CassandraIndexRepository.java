/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.index;

import com.kay.clouds.domain.platform.index.Index;
import com.kay.clouds.domain.platform.index.IndexRepository;
import com.kay.clouds.persistence.CassandraEntityRepository;
import com.kay.clouds.persistence.platform.IndexTable;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;

/**
 *
 * @author Lili
 */
public class CassandraIndexRepository
        extends CassandraEntityRepository<String, Index, CassandraIndex>
        implements IndexRepository {

    public CassandraIndexRepository(PlatformDataBaseHelper dataSourceHelper) {
        mapper = dataSourceHelper.getDataMapper(CassandraIndex.class);
        session = mapper.getManager().getSession();
        keySpace = PlatformDataBaseHelper.DATA_BASE;
        tableName = IndexTable.TABLE_NAME;
        idColumn = IndexTable.TICKER_COLUMN;
    }

    @Override
    protected CassandraIndex parse(Index entity) {
        return CassandraIndex.clone(entity);
    }

}
