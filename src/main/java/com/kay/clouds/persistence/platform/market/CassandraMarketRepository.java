/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.market;

import com.kay.clouds.domain.platform.market.Market;
import com.kay.clouds.domain.platform.market.MarketRepository;
import com.kay.clouds.persistence.CassandraEntityRepository;
import com.kay.clouds.persistence.platform.MarketTable;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;

/**
 * @inheritDoc
 * @author Lili
 */
public class CassandraMarketRepository
        extends CassandraEntityRepository<String, Market, CassandraMarket>
        implements MarketRepository {

    public CassandraMarketRepository(PlatformDataBaseHelper dataSourceHelper) {
        mapper = dataSourceHelper.getDataMapper(CassandraMarket.class);
        session = mapper.getManager().getSession();
        keySpace = PlatformDataBaseHelper.DATA_BASE;
        tableName = MarketTable.TABLE_NAME;
        idColumn = MarketTable.ID_COLUMN;
    }

    @Override
    protected CassandraMarket parse(Market entity) {
        return CassandraMarket.clone(entity);
    }

}
