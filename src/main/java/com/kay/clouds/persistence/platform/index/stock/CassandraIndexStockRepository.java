/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.index.stock;

import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.platform.index.Index;
import com.kay.clouds.domain.platform.index.stock.IndexStockRepository;
import com.kay.clouds.domain.platform.stock.Stock;
import com.kay.clouds.persistence.CassandraRelationKey;
import com.kay.clouds.persistence.CassandraRelationRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.index.CassandraIndexRepository;
import com.kay.clouds.persistence.platform.stock.CassandraStockRepository;

/**
 * @inheritDoc
 * @author Lili
 *
 * @param <RELATION>
 */
public abstract class CassandraIndexStockRepository< RELATION extends CassandraRelationKey<String, String>>
        extends CassandraRelationRepository<String, String, Index, Stock, RELATION>
        implements IndexStockRepository {

    public CassandraIndexStockRepository(PlatformDataBaseHelper dataSourceHelper) {
        fromRepository = new CassandraIndexRepository(dataSourceHelper);
        toRepository = new CassandraStockRepository(dataSourceHelper);
        keySpace = PlatformDataBaseHelper.DATA_BASE;
        fromError = ErrorCode.NOT_FOUND_INDEX;
        toError = ErrorCode.NOT_FOUND_STOCK;
    }

}
