/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform;

import static com.kay.clouds.persistence.CassandraConstants.*;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;

/**
 *
 * @author daniel.eguia
 */
public interface UserFollowStockTable {

    public static final String TABLE_NAME = "userfollowstock";
    public static final String FROM_COLUMN = "userId";
    public static final String TO_COLUMN = "stockticker";

    public static String getTableScheme() {
        return "CREATE TABLE IF NOT EXISTS "
                + DATA_BASE + "." + TABLE_NAME + " ("
                + "PRIMARY KEY (" + FROM_COLUMN + ", " + TO_COLUMN + ") ,"
                + FROM_COLUMN + " " + CASSANDRA_VALUE_TYPE_UUID + ","
                + TO_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + CREATION_TIME_COLUMN + " " + CASSANDRA_VALUE_TYPE_DATE
                + ");";
    }

}
