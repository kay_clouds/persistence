/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.index.stock.contain;

import com.datastax.driver.mapping.annotations.Table;
import com.kay.clouds.persistence.platform.IndexContainStockTable;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;
import com.kay.clouds.persistence.platform.index.stock.CassandraIndexStock;

/**
 *
 * @inheritDoc
 *
 * @author Lili
 */
@Table(keyspace = DATA_BASE, name = IndexContainStockTable.TABLE_NAME)
public class CassandraIndexContainStock extends CassandraIndexStock {

}
