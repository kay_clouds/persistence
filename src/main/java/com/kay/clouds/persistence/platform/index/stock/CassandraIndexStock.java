/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.index.stock;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.kay.clouds.persistence.CassandraRelationKey;
import java.util.Date;

/**
 *
 * @inheritDoc
 *
 * @author Lili
 */
public class CassandraIndexStock implements CassandraRelationKey<String, String> {

    @PartitionKey(0)
    private String indexTicker;
    @PartitionKey(1)
    private String stockTicker;

    private Date creationTime;

    public String getIndexTicker() {
        return indexTicker;
    }

    public void setIndexTicker(String indexTicker) {
        this.indexTicker = indexTicker;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String fromId() {
        return indexTicker;
    }

    @Override
    public String toId() {
        return stockTicker;
    }

    @Override
    public Date creation() {
        return creationTime;
    }

}
