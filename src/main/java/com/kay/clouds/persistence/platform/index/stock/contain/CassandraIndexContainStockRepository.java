/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.index.stock.contain;

import com.kay.clouds.persistence.platform.IndexContainStockTable;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.index.stock.CassandraIndexStockRepository;
import java.util.Date;
import com.kay.clouds.domain.platform.index.stock.IndexContainStockRepository;

/**
 * @inheritDoc
 * @author Lili
 */
public class CassandraIndexContainStockRepository
        extends CassandraIndexStockRepository<CassandraIndexContainStock>
        implements IndexContainStockRepository {

    public CassandraIndexContainStockRepository(PlatformDataBaseHelper dataSourceHelper) {
        super(dataSourceHelper);
        fromKeyColumn = IndexContainStockTable.FROM_COLUMN;
        tableName = IndexContainStockTable.TABLE_NAME;
        relationKeyMapper = dataSourceHelper.getDataMapper(CassandraIndexContainStock.class);
        session = relationKeyMapper.getManager().getSession();
    }

    @Override
    protected CassandraIndexContainStock newKey(String fromKey, String toKey) {
        CassandraIndexContainStock contain = new CassandraIndexContainStock();
        contain.setCreationTime(new Date());
        contain.setIndexTicker(fromKey);
        contain.setStockTicker(toKey);
        return contain;
    }
}
