/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.user.stock.like;

import com.datastax.driver.mapping.annotations.Table;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;
import com.kay.clouds.persistence.platform.user.stock.CassandraUserStock;
import static com.kay.clouds.persistence.platform.UserLikeStockTable.TABLE_NAME;

/**
 *
 * @inheritDoc
 *
 * @author daniel.eguia
 */
@Table(keyspace = DATA_BASE, name = TABLE_NAME)
public class CassandraUserLikeStock extends CassandraUserStock {

}
