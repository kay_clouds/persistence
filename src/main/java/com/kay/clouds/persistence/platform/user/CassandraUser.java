/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.user;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.kay.clouds.domain.platform.user.User;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;
import static com.kay.clouds.persistence.platform.UserTable.TABLE_NAME;
import java.util.UUID;

/**
 *
 * @inheritDoc
 *
 * @author daniel.eguia
 */
@Table(keyspace = DATA_BASE, name = TABLE_NAME)
public class CassandraUser implements User {

    @PartitionKey
    private UUID id;
    private String nickName;

    static CassandraUser clone(User user) {
        CassandraUser cassandraUser = new CassandraUser();
        cassandraUser.id = user.getId();
        cassandraUser.nickName = user.getNickName();
        return cassandraUser;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

}
