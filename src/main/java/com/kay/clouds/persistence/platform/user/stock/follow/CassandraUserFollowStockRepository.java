/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.user.stock.follow;

import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.UserFollowStockTable;
import com.kay.clouds.persistence.platform.user.stock.CassandraUserStockRepository;
import java.util.Date;
import java.util.UUID;
import com.kay.clouds.domain.platform.user.stock.UserFollowStockRepository;

/**
 *
 * @author daniel.eguia
 */
public class CassandraUserFollowStockRepository
        extends CassandraUserStockRepository<CassandraUserFollowStock>
        implements UserFollowStockRepository {

    public CassandraUserFollowStockRepository(PlatformDataBaseHelper dataSourceHelper) {
        super(dataSourceHelper);
        fromKeyColumn = UserFollowStockTable.FROM_COLUMN;
        tableName = UserFollowStockTable.TABLE_NAME;
        relationKeyMapper = dataSourceHelper.getDataMapper(CassandraUserFollowStock.class);
        session = relationKeyMapper.getManager().getSession();
    }

    @Override
    protected CassandraUserFollowStock newKey(UUID fromKey, String toKey) {
        CassandraUserFollowStock follow = new CassandraUserFollowStock();
        follow.setCreationTime(new Date());
        follow.setStockTicker(toKey);
        follow.setUserId(fromKey);
        return follow;
    }

}
