/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.user.stock;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.kay.clouds.persistence.CassandraRelationKey;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @inheritDoc
 * @author Lili
 * @author daniel.eguia
 */
public class CassandraUserStock implements CassandraRelationKey<UUID, String> {

    @PartitionKey(0)
    private UUID userId;
    @PartitionKey(1)
    private String stockTicker;
    private Date creationTime;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    @Override
    public UUID fromId() {
        return userId;
    }

    @Override
    public String toId() {
        return stockTicker;
    }

    @Override
    public Date creation() {
        return creationTime;
    }

}
