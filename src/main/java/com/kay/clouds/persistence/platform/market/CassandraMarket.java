/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.market;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.kay.clouds.domain.platform.market.Market;
import com.kay.clouds.persistence.platform.MarketTable;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;

/**
 * @inheritDoc
 * @author Lili
 */
@Table(keyspace = DATA_BASE, name = MarketTable.TABLE_NAME)
public class CassandraMarket implements Market {

    @PartitionKey
    private String id;

    private String name;

    private String countryCode;

    private String currencyCode;

    private String description;

    private Double latitude;

    private Double longitude;

    static CassandraMarket clone(Market market) {
        CassandraMarket cassandraMarket = new CassandraMarket();
        cassandraMarket.countryCode = market.getCountryCode();
        cassandraMarket.currencyCode = market.getCurrencyCode();
        cassandraMarket.description = market.getDescription();
        cassandraMarket.id = market.getId();
        cassandraMarket.latitude = market.getLatitude();
        cassandraMarket.longitude = market.getLongitude();
        cassandraMarket.name = market.getName();
        return cassandraMarket;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Override
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
