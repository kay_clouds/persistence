/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform;

import static com.kay.clouds.persistence.CassandraConstants.*;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;

/**
 * @author Lili
 */
public interface MarketContainIndexTable {

    public static final String TABLE_NAME = "marketcontainindex";
    public static final String FROM_COLUMN = "marketId";
    public static final String TO_COLUMN = "indexTicker";

    public static String getTableScheme() {
        return "CREATE TABLE IF NOT EXISTS "
                + DATA_BASE + "." + TABLE_NAME + " ("
                + "PRIMARY KEY (" + FROM_COLUMN + ", " + TO_COLUMN + ") ,"
                + FROM_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + TO_COLUMN + " " + CASSANDRA_VALUE_TYPE_STRING + ","
                + CREATION_TIME_COLUMN + " " + CASSANDRA_VALUE_TYPE_DATE
                + ");";
    }

}
