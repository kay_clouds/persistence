/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.stock;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.kay.clouds.domain.platform.stock.Stock;
import static com.kay.clouds.persistence.platform.PlatformDataBaseHelper.DATA_BASE;
import com.kay.clouds.persistence.platform.StockTable;

/**
 *
 * @inheritDoc
 *
 * @author Lili
 */
@Table(keyspace = DATA_BASE, name = StockTable.TABLE_NAME)
public class CassandraStock implements Stock {

    @PartitionKey
    private String ticker;
    private String company;
    private String companyUrl;
    private String industry;
    private String countryCode;
    private String sector;
    private Long shareNumber;
    private String market;
    private String currencyCode;

    static CassandraStock clone(Stock stock) {
        CassandraStock cassandraStock = new CassandraStock();
        cassandraStock.ticker = stock.getTicker();
        cassandraStock.company = stock.getCompany();
        cassandraStock.industry = stock.getIndustry();
        cassandraStock.countryCode = stock.getCountryCode();
        cassandraStock.sector = stock.getSector();
        cassandraStock.shareNumber = stock.getShareNumber();
        cassandraStock.companyUrl = stock.getCompanyUrl();
        cassandraStock.market = stock.getMarket();
        cassandraStock.currencyCode = stock.getCurrencyCode();
        return cassandraStock;
    }

    @Override
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @Override
    public Long getShareNumber() {
        return shareNumber;
    }

    public void setShareNumber(Long shareNumber) {
        this.shareNumber = shareNumber;
    }

    @Override
    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    @Override
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    @Override
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    @Override
    public String getCompanyUrl() {
        return companyUrl;
    }

    @Override
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

}
