/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kay.clouds.persistence.platform.user.stock;

import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.platform.stock.Stock;
import com.kay.clouds.domain.platform.user.User;
import com.kay.clouds.domain.platform.user.stock.UserStockRepository;
import com.kay.clouds.persistence.CassandraRelationKey;
import com.kay.clouds.persistence.CassandraRelationRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.stock.CassandraStockRepository;
import com.kay.clouds.persistence.platform.user.CassandraUserRepository;
import java.util.UUID;

/**
 *
 * @author Lili
 * @param <RELATION>
 */
public abstract class CassandraUserStockRepository< RELATION extends CassandraRelationKey<UUID, String>>
        extends CassandraRelationRepository<UUID, String, User, Stock, RELATION>
        implements UserStockRepository {

    public CassandraUserStockRepository(PlatformDataBaseHelper dataSourceHelper) {
        fromRepository = new CassandraUserRepository(dataSourceHelper);
        toRepository = new CassandraStockRepository(dataSourceHelper);
        keySpace = PlatformDataBaseHelper.DATA_BASE;
        fromError = ErrorCode.NOT_FOUND_USER;
        toError = ErrorCode.NOT_FOUND_STOCK;
    }

}
