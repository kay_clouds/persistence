/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.security;

import com.kay.clouds.domain.security.credential.Credential;
import com.kay.clouds.domain.security.credential.CredentialRepository;
import com.kay.clouds.persistence.CassandraEntityRepository;
import static com.kay.clouds.persistence.security.CassandraCredentialTable.EMAIL_COLUMN;
import java.util.UUID;

/**
 *
 * @author Lili
 */
public class CassandraCredentialRepository
        extends CassandraEntityRepository<UUID, Credential, CassandraCredential>
        implements CredentialRepository {

    public CassandraCredentialRepository(
            CassandraSecurityDataBaseHelper dataSourceHelper) {

        mapper = dataSourceHelper.getDataMapper(CassandraCredential.class);
        session = mapper.getManager().getSession();
        keySpace = CassandraSecurityDataBaseHelper.DATA_BASE;
        tableName = CassandraCredentialTable.TABLE_NAME;
        idColumn = CassandraCredentialTable.ID_COLUMN;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public Credential findByEmail(String email) {
        return find(EMAIL_COLUMN, email);
    }

    @Override
    protected CassandraCredential parse(Credential entity) {
        return CassandraCredential.clone(entity);
    }

}
