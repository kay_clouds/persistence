/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.market.index.contain;

import com.kay.clouds.domain.error.ErrorException;
import com.kay.clouds.domain.platform.index.IndexRepository;
import com.kay.clouds.domain.platform.market.MarketRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.index.CassandraIndex;
import com.kay.clouds.persistence.platform.index.CassandraIndexRepository;
import com.kay.clouds.persistence.platform.market.CassandraMarket;
import com.kay.clouds.persistence.platform.market.CassandraMarketRepository;
import java.io.IOException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import com.kay.clouds.domain.platform.market.index.MarketContainIndexRepository;

/*
 * @author Lili
 */
public class MarketContainIndexRepositoryTest {

    public static final long CASSANDRA_TIMEOUT = 20000L;

    private MarketContainIndexRepository repository;

    private CassandraIndex index;
    private CassandraMarket market;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra(CASSANDRA_TIMEOUT);
        PlatformDataBaseHelper helper = PlatformDataBaseHelper.getCassandraDataSourceHelper(9142, "localhost");
        repository = new CassandraMarketContainIndexRepository(helper);
        index = new CassandraIndex();
        index.setTicker("index ticker");
        market = new CassandraMarket();
        market.setId("market id");

        IndexRepository indexRepository = new CassandraIndexRepository(helper);
        indexRepository.save(index);
        MarketRepository marketRepository = new CassandraMarketRepository(helper);
        marketRepository.save(market);
    }

    @Test
    public void repositoryBasicOperationTest() throws ErrorException {
        repository.create(market.getId(), index.getId());
        assertThat(repository.all(market.getId()).size()).isEqualTo(1);
    }

}
