/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.index.stock.contain;

import com.kay.clouds.domain.error.ErrorException;
import com.kay.clouds.domain.platform.index.IndexRepository;
import com.kay.clouds.domain.platform.stock.StockRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.index.CassandraIndex;
import com.kay.clouds.persistence.platform.index.CassandraIndexRepository;
import com.kay.clouds.persistence.platform.stock.CassandraStock;
import com.kay.clouds.persistence.platform.stock.CassandraStockRepository;
import java.io.IOException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import com.kay.clouds.domain.platform.index.stock.IndexContainStockRepository;

/*
 * @author Lili
 */
public class IndexContainStockRepositoryTest {

    public static final long CASSANDRA_TIMEOUT = 20000L;

    private IndexContainStockRepository repository;

    private CassandraIndex index;
    private CassandraStock stock;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra(CASSANDRA_TIMEOUT);
        PlatformDataBaseHelper helper = PlatformDataBaseHelper.getCassandraDataSourceHelper(9142, "localhost");
        repository = new CassandraIndexContainStockRepository(helper);
        index = new CassandraIndex();
        index.setTicker("index ticker");
        stock = new CassandraStock();
        stock.setTicker("stock ticker");

        IndexRepository indexRepository = new CassandraIndexRepository(helper);
        indexRepository.save(index);
        StockRepository stockRepository = new CassandraStockRepository(helper);
        stockRepository.save(stock);
    }

    @Test
    public void repositoryBasicOperationTest() throws ErrorException {
        repository.create(index.getId(), stock.getId());
        assertThat(repository.all(index.getId()).size()).isEqualTo(1);
    }

}
