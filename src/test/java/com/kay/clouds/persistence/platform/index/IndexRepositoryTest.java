/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.index;

import com.kay.clouds.domain.platform.index.IndexRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import java.io.IOException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;

/*
 * @author Lili
 */
public class IndexRepositoryTest {

    private IndexRepository repository;
    private CassandraIndex index;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra();
        PlatformDataBaseHelper helper = PlatformDataBaseHelper.getCassandraDataSourceHelper(9142, "localhost");
        repository = new CassandraIndexRepository(helper);
        index = new CassandraIndex();
        index.setCurrencyCode("test currency code");
        index.setDescription("test desription ");
        index.setTicker("test id");
        index.setName("test name");

    }

    @Test
    public void repositoryBasicOperationTest() {
        repository.save(index);
        assertThat(repository.all().size()).isEqualTo(1);

    }

}
