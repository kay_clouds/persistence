/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.user.stock.like;

import com.kay.clouds.domain.error.ErrorException;
import com.kay.clouds.domain.platform.stock.StockRepository;
import com.kay.clouds.domain.platform.user.User;
import com.kay.clouds.domain.platform.user.UserRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.stock.CassandraStock;
import com.kay.clouds.persistence.platform.stock.CassandraStockRepository;
import com.kay.clouds.persistence.platform.user.CassandraUser;
import com.kay.clouds.persistence.platform.user.CassandraUserRepository;
import java.io.IOException;
import java.util.UUID;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import com.kay.clouds.domain.platform.user.stock.UserLikeStockRepository;

/*
 * @author Lili
 */
public class UserLikeStockRepositoryTest {

    public static final long CASSANDRA_TIMEOUT = 20000L;

    private UserLikeStockRepository likeRepository;

    private User user;
    private CassandraStock stock;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra(CASSANDRA_TIMEOUT);
        PlatformDataBaseHelper helper = PlatformDataBaseHelper.getCassandraDataSourceHelper(9142, "localhost");
        likeRepository = new CassandraUserLikeStockRepository(helper);
        user = new CassandraUser();
        user.setId(UUID.randomUUID());
        stock = new CassandraStock();
        stock.setTicker("ticker");

        UserRepository userRepository = new CassandraUserRepository(helper);
        userRepository.save(user);
        StockRepository stockRepository = new CassandraStockRepository(helper);
        stockRepository.save(stock);
    }

    @Test
    public void repositoryBasicOperationTest() throws ErrorException {
        likeRepository.create(user.getId(), stock.getId());
        assertThat(likeRepository.all(user.getId()).size()).isEqualTo(1);
    }

}
