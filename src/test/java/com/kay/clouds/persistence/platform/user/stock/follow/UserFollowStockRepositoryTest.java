/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.user.stock.follow;

import com.kay.clouds.domain.error.ErrorException;
import com.kay.clouds.domain.platform.stock.StockRepository;
import com.kay.clouds.domain.platform.user.User;
import com.kay.clouds.domain.platform.user.UserRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.stock.CassandraStock;
import com.kay.clouds.persistence.platform.stock.CassandraStockRepository;
import com.kay.clouds.persistence.platform.user.CassandraUser;
import com.kay.clouds.persistence.platform.user.CassandraUserRepository;
import java.io.IOException;
import java.util.UUID;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import com.kay.clouds.domain.platform.user.stock.UserFollowStockRepository;

/*
 * @author Lili
 */
public class UserFollowStockRepositoryTest {

    public static final long CASSANDRA_TIMEOUT = 20000L;

    private UserFollowStockRepository followRepository;

    private User user;
    private CassandraStock stock;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra(CASSANDRA_TIMEOUT);
        PlatformDataBaseHelper helper = PlatformDataBaseHelper.getCassandraDataSourceHelper(9142, "localhost");
        followRepository = new CassandraUserFollowStockRepository(helper);
        user = new CassandraUser();
        user.setId(UUID.randomUUID());
        stock = new CassandraStock();
        stock.setTicker("ticker");

        UserRepository userRepository = new CassandraUserRepository(helper);
        userRepository.save(user);
        StockRepository stockRepository = new CassandraStockRepository(helper);
        stockRepository.save(stock);
    }

    @Test
    public void repositoryBasicOperationTest() throws ErrorException {
        followRepository.create(user.getId(), stock.getId());
        assertThat(followRepository.all(user.getId()).size()).isEqualTo(1);
    }

}
