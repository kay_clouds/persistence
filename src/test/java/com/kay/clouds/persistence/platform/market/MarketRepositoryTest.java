/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.market;

import com.kay.clouds.domain.platform.market.MarketRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import java.io.IOException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;

/*
 * @author Lili
 */
public class MarketRepositoryTest {

    private MarketRepository repository;
    private CassandraMarket market;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra();
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
        PlatformDataBaseHelper helper = PlatformDataBaseHelper.getCassandraDataSourceHelper(9142, "localhost");
        repository = new CassandraMarketRepository(helper);
        market = new CassandraMarket();
        market.setCountryCode("test country code");
        market.setCurrencyCode("test currency code");
        market.setDescription("test desription ");
        market.setId("test id");
        market.setLatitude(10.0);
        market.setLongitude(1.0);
        market.setName("test name");

    }

    @Test
    public void repositoryBasicOperationTest() {
        repository.save(market);
        assertThat(repository.all().size()).isEqualTo(1);

        market.setCountryCode("test country code 2");
        market.setId("test id 2");
        repository.save(market);
        assertThat(repository.all().size()).isEqualTo(2);
    }

}
