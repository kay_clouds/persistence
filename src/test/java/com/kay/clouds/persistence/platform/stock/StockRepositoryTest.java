/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.platform.stock;

import com.kay.clouds.domain.platform.stock.StockRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import java.io.IOException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;

/*
 * @author Lili
 */
public class StockRepositoryTest {

    private StockRepository stockRepository;
    private CassandraStock stock;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra();
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
        PlatformDataBaseHelper helper = PlatformDataBaseHelper.getCassandraDataSourceHelper(9142, "localhost");
        stockRepository = new CassandraStockRepository(helper);
        stock = new CassandraStock();
        stock.setCompany("testCompany");
        stock.setCountryCode("testCountry");
        stock.setIndustry("testIndustry");
        stock.setTicker("test.ticker");

    }

    @Test
    public void repositoryBasicOperationTest() {
        stockRepository.save(stock);
        assertThat(stockRepository.all().size()).isEqualTo(1);
        assertThat(stockRepository.one(stock.getTicker())).isNotNull();
    }

}
