/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.security;

import com.google.common.collect.Lists;
import com.kay.clouds.domain.security.Permission;
import com.kay.clouds.domain.security.token.TokenRepository;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * @author Lili
 */
public class TokenRepositoryTest {

    private TokenRepository tokenRepository;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra();
        CassandraSecurityDataBaseHelper helper = CassandraSecurityDataBaseHelper.getCassandraDataSourceHelper(9142, "localhost");
        tokenRepository = new CassandraTokenRepository(helper);
    }

    @Test
    public void repositoryBasicOperationTest() throws InterruptedException {

        Integer ttl = 10;

        UUID id = tokenRepository.create(Lists.newArrayList(Permission.ANALYTICS), ttl, UUID.randomUUID());

        assertThat(tokenRepository.exist(id, Permission.PLATFORM)).isNull();
        assertThat(tokenRepository.exist(UUID.randomUUID(), Permission.PLATFORM)).isNull();
        assertThat(tokenRepository.exist(id, Permission.ANALYTICS)).isNotNull();

        // wait token to die
        TimeUnit.SECONDS.sleep(ttl);
        assertThat(tokenRepository.exist(id, Permission.ANALYTICS)).isNull();
    }

    @After
    public void shutdown() {
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
    }

}
