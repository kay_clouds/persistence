/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics.exchange.stock;

import com.kay.clouds.persistence.analytics.exchange.stock.CassandraStockExchange;
import com.kay.clouds.persistence.analytics.exchange.stock.CassandraStockExchangeRepository;
import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.StockExchangeRepository;
import com.kay.clouds.persistence.analytics.DataWarehouseHelper;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Lili
 */
public class StockExchangeRepositoryTest {

    private StockExchangeRepository stockExchangeRepository;
    private CassandraStockExchange stockExchange;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {

        EmbeddedCassandraServerHelper.startEmbeddedCassandra();
        DataWarehouseHelper helper = DataWarehouseHelper.getCassandraDataSourceHelper(9142, "localhost");
        stockExchangeRepository = new CassandraStockExchangeRepository(helper);
        stockExchange = new CassandraStockExchange();
        stockExchange.setAdjustedclosingPrice(90.2);
        stockExchange.setClosingPrice(88.9);
        stockExchange.setDailyMaxPrice(245.5);
        stockExchange.setDate(new Date());
        stockExchange.setExchangeVolume(2342.6);
        stockExchange.setOpenningPrice(2324.6);
        stockExchange.setTicker("test.Stock");

    }

    @Test
    public void repositoryBasicOperationTest() {
        stockExchangeRepository.save(stockExchange);
        stockExchange = new CassandraStockExchange();
        stockExchange.setAdjustedclosingPrice(90.2);
        stockExchange.setClosingPrice(88.9);
        stockExchange.setDailyMaxPrice(245.5);
        stockExchange.setDate(new Date());
        stockExchange.setExchangeVolume(2342.6);
        stockExchange.setOpenningPrice(2324.6);
        stockExchange.setTicker("test.Stock");
        stockExchangeRepository.save(stockExchange);

        stockExchange = new CassandraStockExchange();
        stockExchange.setAdjustedclosingPrice(90.2);
        stockExchange.setClosingPrice(88.9);
        stockExchange.setDailyMaxPrice(245.5);
        stockExchange.setDate(new Date());
        stockExchange.setExchangeVolume(2342.6);
        stockExchange.setOpenningPrice(2324.6);
        stockExchange.setTicker("test.Stock");
        stockExchangeRepository.save(stockExchange);

        //should get stock with date 3l and 2l, and the order is 3l, 2l.
        List<? extends Exchange> stockExchageList = stockExchangeRepository.getDescendantListBelow("test.Stock", new Date(), 2);

        assertThat(stockExchageList.size()).isEqualTo(2);
        assertThat(
                stockExchageList.get(0).getDate()
                        .after(stockExchageList.get(1).getDate()))
                .isTrue();

    }

    @After
    public void shutdown() {
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
    }

}
