/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.persistence.analytics.exchange.index;

import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.IndexExchangeRepository;
import com.kay.clouds.persistence.analytics.DataWarehouseHelper;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Lili
 */
public class IndexExchangeRepositoryTest {

    private IndexExchangeRepository indexExchangeRepository;
    private CassandraIndexExchange indexExchange;

    @Before
    public void before() throws TTransportException, IOException, InterruptedException {

        EmbeddedCassandraServerHelper.startEmbeddedCassandra(20000l);
        DataWarehouseHelper helper = DataWarehouseHelper.getCassandraDataSourceHelper(9142, "localhost");
        indexExchangeRepository = new CassandraIndexExchangeRepository(helper);
        indexExchange = new CassandraIndexExchange();
        indexExchange.setAdjustedclosingPrice(90.2);
        indexExchange.setClosingPrice(88.9);
        indexExchange.setDailyMaxPrice(245.5);
        indexExchange.setDate(new Date());
        indexExchange.setExchangeVolume(2342.6);
        indexExchange.setOpenningPrice(2324.6);
        indexExchange.setTicker("index");

    }

    @Test
    public void repositoryBasicOperationTest() {
        indexExchangeRepository.save(indexExchange);
        indexExchange = new CassandraIndexExchange();
        indexExchange.setAdjustedclosingPrice(90.2);
        indexExchange.setClosingPrice(88.9);
        indexExchange.setDailyMaxPrice(245.5);
        indexExchange.setDate(new Date());
        indexExchange.setExchangeVolume(2342.6);
        indexExchange.setOpenningPrice(2324.6);
        indexExchange.setTicker("test.Stock");
        indexExchangeRepository.save(indexExchange);

        indexExchange = new CassandraIndexExchange();
        indexExchange.setAdjustedclosingPrice(90.2);
        indexExchange.setClosingPrice(88.9);
        indexExchange.setDailyMaxPrice(245.5);
        indexExchange.setDate(new Date());
        indexExchange.setExchangeVolume(2342.6);
        indexExchange.setOpenningPrice(2324.6);
        indexExchange.setTicker("test.Stock");
        indexExchangeRepository.save(indexExchange);

        //should get index with date 3l and 2l, and the order is 3l, 21.
        List<? extends Exchange> indexExchageList = indexExchangeRepository.getDescendantListBelow("test.Stock", new Date(), 2);

        assertThat(indexExchageList.size()).isEqualTo(2);
        assertThat(
                indexExchageList.get(0).getDate()
                        .after(indexExchageList.get(1).getDate()))
                .isTrue();

    }

}
